# NodeJS Challenges - CommonJS Pattern & Recursion

## Objectives:

- To learn about `Recursion`.
- To learn about `CommonJS Module Pattern`.
- To learn for seeking information, reading documentation and checking out topics 
  about `NodeJS`.


### Theory & Documentation

# Recursion

In simple terms, `recursion` is when a function keeps calling itself, until it no longer has to.

`Recursion` is a `programming pattern` that is useful in situations when a task can be naturally split into several tasks of the same kind, but simpler. Or when a task can be simplified into an easy action plus a simpler variant of the same task. Or also, to deal with certain data structures.

## How recursion works

Let's see how recursion can magically find the `factorial` of any number. 

```javascript
const factorial = (n) => {
    if (n === 0) return 1

    return n * factorial(n-1)
}
```

`factorial()` returns `1` when `n` is `0`, otherwise it returns the product of `n` and the factorial of `n - 1`.

Every recursive procedure uses conditionals, and will need two cases:

- `Base case`: This case ends the recursion. Any input to a 
  recursive procedure will eventually reach the base case.
- `Recursive case`: This case reduces the size of the problem. The recursive case 
  will always try to make the problem smaller until it reaches the base case.

## Example 1

Let’s write a function `pow(x, n)` that raises `x` to a natural power of `n`. In other words, multiplies `x` by itself `n` times.

```javascript
pow(2, 2)   //4
pow(2, 3)   //8
pow(2, 4)   //16
```

```javascript
function pow(x, n) {
    return (n === 1) ? x : (x * pow(x, n - 1))
}
```

When `pow(x, n)` is called, the execution splits into two branches:

```
              if n==1  = x
             /
pow(x, n) =
             \
              else      = x * pow(x, n - 1)

```

1. If `n === 1`, then everything is trivial. It is called the base of recursion, 
   because it immediately produces the obvious result: `pow(x, 1)` equals `x`.

2. Otherwise, we can represent `pow(x, n)` as `x * pow(x, n - 1)`. In maths, one would write `x ** n` = `x * (x ** (n-1))`. This is called a `recursive step`: we transform the task into a simpler action (multiplication by x) and a simpler call of the same task (pow with lower n). Next steps simplify it further and further until `n` reaches `1`.

We can also say that `pow()` recursively calls itself till `n == 1`.

For example, to calculate `pow(2, 4)` the recursive variant does these steps:

```
pow(2, 4) = 2 * pow(2, 3)
pow(2, 3) = 2 * pow(2, 2)
pow(2, 2) = 2 * pow(2, 1)
pow(2, 1) = 2
```
So, the `recursion` reduces a function call to a simpler one, and then – to even more simpler, and so on, until the result becomes obvious.

> A recursive solution is usually shorter than an iterative one.

The maximal number of nested calls (including the first one) is called `recursion depth`. In our case, it will be exactly n.

The maximal `recursion depth` is limited by JavaScript engine. We can rely on it being 10000, some engines allow more, but 100000 is probably out of limit for the majority of them. There are automatic optimizations that help alleviate this (“tail calls optimizations”), but they are not yet supported everywhere and work only in simple cases.

That limits the application of recursion, but it still remains very wide. There are many tasks where recursive way of thinking gives simpler code, easier to maintain.


## The execution context and stack

Now let’s examine how recursive calls work. For that we’ll look under the hood of functions.

The information about the process of execution of a running function is stored in its `execution context`.

The `execution context` is an internal data structure that contains details about the execution of a function: where the control flow is now, the current variables, the value of `this`` (we don’t use it here) and few other internal details.

> One function call has exactly one `execution context` associated with it.

When a function makes a nested call, the following happens:

- The current function is paused.
- The execution context associated with it is remembered in a special data 
  structure called `execution context stack`.
- The nested call executes.
- After it ends, the old execution context is retrieved from the stack, and the 
  outer function is resumed from where it stopped.

Let’s see what happens during the `pow(2, 3)` call:

In the beginning of the call `pow(2, 3)` the `execution context` will store variables: `x = 2, n = 3`, the execution flow is at line `1` of the function.

We can sketch it as:

```
Context: { x: 2, n: 3, at line 1 } call: pow(2, 3)
```

That’s when the function starts to execute. The condition `n === 1` is falsy, so the flow continues into the second branch of `if`:

```javascript
function pow(x, n) {
  if (n === 1) {
    return x
  } else {
    //here second branch
    return x * pow(x, n - 1)
  }
}
```

The variables are same, but the line changes, so the context is now:

```
Context: { x: 2, n: 3, at line 5 } call: pow(2, 3)
```

To calculate `x * pow(x, n - 1)`, we need to make a subcall of `pow()` with new arguments `pow(2, 2)`.

### pow(2, 2)

To do a nested call, JavaScript remembers the current `execution context` in the `execution context stack`.

Here we call the same function `pow()`, but it absolutely doesn’t matter. The process is the same for all functions:

1. The current context is “remembered” on top of the stack.
2. The new context is created for the subcall.
3. When the subcall is finished – the previous context is popped from the stack, 
   and its execution continues.

Here’s the context stack when we entered the subcall `pow(2, 2)`:

```
Context: { x: 2, n: 2, at line 1 } call: pow(2, 2)    //current context
Context: { x: 2, n: 3, at line 5 } call: pow(2, 3)    //previous context
```

The new `current execution context` is on `top`, and previous remembered contexts are below.

When we finish the subcall – it is easy to resume the previous context, because it keeps both variables and the exact place of the code where it stopped.

### pow(2, 1)

The process repeats: a new subcall is made at `line 5`, now with arguments `x=2`, `n=1`.

A new `execution context` is created, the `previous one` is pushed on top of the stack:

```
Context: { x: 2, n: 1, at line 1 } call: pow(2, 1)    //current context
Context: { x: 2, n: 2, at line 5 } call: pow(2, 2)    //previous context
Context: { x: 2, n: 3, at line 5 } call: pow(2, 3)    //old context
```
There are 2 old contexts now and 1 currently running for `pow(2, 1)`.

### The exit

During the execution of `pow(2, 1)`, unlike before, the condition `n === 1` is truthy, so the first branch of `if` works:

```javascript
function pow(x, n) {
  if (n === 1) {
    //here first branch
    return x;
  } else {
    return x * pow(x, n - 1);
  }
}
```

There are no more nested calls, so the function finishes, returning `2`.

As the function finishes, its `execution context` is not needed anymore, so it’s removed from the memory. The previous one is restored off the top of the stack:

```
Context: { x: 2, n: 2, at line 5 } call: pow(2, 2)
Context: { x: 2, n: 3, at line 5 } call: pow(2, 3)
```

The execution of `pow(2, 2)` is resumed. It has the result of the subcall `pow(2, 1)`, so it also can finish the evaluation of `x * pow(x, n - 1)`, returning `4`.

Then the `previous context` is restored:

```
Context: { x: 2, n: 3, at line 5 } call: pow(2, 3)
```

When it finishes, we have a result of `pow(2, 3) = 8`.

The `recursion depth` in this case was: `3`.

### Recursion vs Loops 

As we can see from the illustrations above, `recursion depth` equals the maximal number of contexts in the stack.

Note the memory requirements. `Contexts take memory`. In our case, raising to the `power of n` actually requires the memory for `n contexts`, for all lower values of `n`.

> When it comes to speed, a loop executes much faster than a recursive function.

A loop-based algorithm is more memory-saving:

```javascript
function pow(x, n) {
  let result = 1;

  for (let i = 0; i < n; i++) {
    result *= x;
  }

  return result;
}
```

The iterative `pow()` uses a single context changing `i` and `result` in the process. Its memory requirements are small, fixed and do not depend on `n`.

> Any recursion can be rewritten as a loop. The loop variant usually can be made more effective.

…But sometimes the rewrite is non-trivial, especially when function uses different recursive subcalls depending on conditions and merges their results or when the branching is more intricate. And the optimization may be unneeded and totally not worth the efforts.

`Recursion` can give a shorter code, easier to understand and support. Optimizations are not required in every place, mostly we need a good code, that’s why it’s used.

#### So what is the best option? Efficiency or speed?

This is a sentence from the `Eloquent Javascript book`:

> Worrying about efficiency can be distracting. It's yet another factor that complicates program design, and when you're doing something that's already difficult, that extra thing to worry about can be crippling. Therefore, always start by writing something that is correct and easy to understand. If you're concerned that it's too slow, which it usually isn't, since most code just doesn't run often enough to take a significant amount of time, you can measure it later and improve it if necessary.

Why someone would choose to write a `recursive function` instead of a `loop`. If the loops are much simpler, right?

> There are some problems that are easier to solve with recursion.

## Example 2 

`sum` of an array using recursion Javascript:

```javascript
var sum = function(array) {
    return (array.length === 0) ? 0 : array[0] + sum(array.slice(1));
}

// or in ES6

const sum = (array) => (array.length === 0) ? 0 : array[0] + sum(array.slice(1));

// Test cases
sum([1,2,3]); // 6

const s = [1,2,3]

sum(s); // 6
sum(s); // 6
```

Steps:

- In a recursive call, you need to model your task as reduction to a `base case`. 
  The simplest `base case` in this case is the empty array - at that point, your function should return zero.
- What should the `reduction step` be? Well you can model a sum of an array as the 
  result of adding the first element to the sum of the remainder of the array - at some point, these successive calls will eventually result in a call to `sum([])`, the answer to which you already know. That is exactly what the code above does.
- `array.slice(1)` creates a shallow copy of the array starting from the first 
  element onwards, and no mutation ever occurs on the original array. 

Breakdown `recursion depth`:

```
sum([1,2,3])
-> 1 + sum([2,3])
-> 1 + 2 + sum([3])
-> 1 + 2 + 3 + sum([])
-> 1 + 2 + 3 + 0
-> 6
```

## Example 3 - Recursive traversals

Another great application of the recursion is a `recursive traversal`.

Imagine, we have a company. The staff structure can be presented as an object:

```javascript
let company = { 
  sales: [{name: 'John', salary: 1000}, {name: 'Alice', salary: 1600 }],
  development: {
    sites: [{name: 'Peter', salary: 2000}, {name: 'Alex', salary: 1800 }],
    internals: [{name: 'Jack', salary: 1300}]
  }
}
```

Now let’s say we want a function to get the sum of all salaries. How can we do that?

When our function gets a department to sum, there are two possible cases:

1. Either it’s a “simple” department with an array of people – then we can sum the 
   salaries in a simple loop.
2. Or it’s an object with `N` subdepartments – then we can make `N` recursive 
   calls to get the sum for each of the subdeps and combine the results.

The `1st case` is the base of recursion, the trivial case, when we get an array.

The `2nd case` when we get an object is the recursive step. A complex task is split into subtasks for smaller departments. They may in turn split again, but sooner or later the split will finish at (1).

Let's see the function to do the job:

```javascript
function sumSalaries(department) {
  if (Array.isArray(department)) { // case (1)
    return department.reduce((prev, current) => prev + current.salary, 0); // sum the array
  } else { // case (2)
    let sum = 0;
    for (let subdep of Object.values(department)) {
      sum += sumSalaries(subdep); // recursively call for subdepartments, sum the results
    }
    return sum;
  }
}

console.log(sumSalaries(company))    // 7700
```

We can easily see the principle: 

- for an `object {...}` subcalls are made, while `arrays [...]` are the “leaves” 
  of the recursion tree, they give immediate result.

- The code uses smart features that we’ve covered before:

- Method `arr.reduce` to get the sum of the array.

- Loop `for(val of Object.values(obj))` to iterate over object values: 
  `Object.values` returns an array of them.


# Recursion Challenges (Instructions) - Solving Problems

### Description

1) Answer the following questions about Recursion:

The case in which `n` is `0` is the base case of `factorial()`. Consider this alternate definition of factorial, which has no base case:

```javascript
const factorial = (n) => {
    return n * factorial(n-1)
}
```

What is wrong with this alternate definition?

1. It no longer handles the special case of 0! being defined as 1.
2. It always returns 0.
3. It causes an error as soon as n reaches 0.
4. It loops indefinitely.

The second case in which we call `factorial()` within itself is the recursive case. Notice that the recursive call solves a smaller problem (i.e., `factorial(n-1)`) than the one we were originally given. Consider this alternate definition of factorial:

```javascript
const factorial = (n) => {
    if (n === 0) return 1

    return n * factorial(n)
}
```

What's wrong with this alternate definition?

1. It causes an error because you cannot define n! in terms of itself. 
2. It always returns 1.
3. It keeps multiplying n by itself indefinitely.
4. It loops indefinitely without computing anything. 

2) Define a recursive function `sumString` that receives as argument a string and returns the sum of each value. Test must be true.

```javascript
//ES6
//sumString()


console.log(sumString("123456789") === 45)
```

3)  Define a recursive function `recursionString` to reverse a string. Test must be true.

```javascript
//ES6
//recursionString()


console.log(recursionString("margorpsjedon") === "nodejsprogram")
```


<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />


> References:

1. [Recursion in Javascript](https://www.freecodecamp.org/espanol/news/como-entender-recursividad-en-javascript/)

2. [Recursion](https://javascript.info/recursion)

3. [Slice Method](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/slice)

4. [How Recursion Works](https://berkeley-cs61as.github.io/textbook/how-recursion-works.html)

5. [Sum of an Array Using Recursion](https://stackoverflow.com/questions/37425581/sum-of-an-array-using-recursion-javascript/37425626)
